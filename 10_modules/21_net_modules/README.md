# Python Script Network Modules

.footer: Created by Alex M. Schapelle, VaioLabs.IO

---
# IPADDRESS/Tabulate

`ipaddress` library enables us to create ipv4 or ipv6 addresses
It is mostly used for checking ip address configurations natively with python.

```py
import ipaddress

ip1 = ipaddress.ip_address
```

---
# JSON

---

# YAML


---
# XMLTODICT


---

# PEXPECT


---

# Telnetlib


---

# PARAMIKO

---

# NETMIKO

---

