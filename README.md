# Python Script

## Python Programming 

## Programming With Open Source

In todays software development we tend to combine between our wish to develop fast and to develop secure.
Fortunately there are a lot of programming languages and development ecosystems that provide that possibility. 
One of those languages is `Python`: 
`Open Source General Purpose Programming Language` with broad standard library and large community of developers and contributors.

## Who Is This Course For ?

- Software developers who would like to learn python.
- Junior/senior sysadmins who have no knowledge programming and automation with python.
- DevOps engineers who would like to learn software development.
- SRE engineers who would like educate themselves with python.


## Course Topics

- Scripting intro
  - History.
  - Types of programming languages.
  - What is Python ?
  - How Python works ?
  - What is required for Python?
  - Environment setup.
  - What to do with code ? <!--Version Control Basics -->
  - REPL.
  - Python syntax.
  - Variables in Python.
  - Types of data in general and in Python.
  - Casting of data types.
- Scripting parameters
  - STDIN, STDOUT and STDERR.
  - Passing values to Python scripts.
  - Advanced value passing to python scripts.
- Scripting conditions
  - if..elif..else
- Storing data
  - Lists of data types.
  - Useless Lists: Tuples.
  - Lists with unique data: Sets.
  - When Lists are not enough: Dictionaries.
- Scripting loops
  - Serial loop: for.
  - Condition loop: while.
- Functions
  - What is Function ?
  - Why we need them ?
  - Built-in functions. 
  - Customs functions
  - Magic functions.
  - Functions in libraries.
- Working with files and exceptions
  - Try and Exceptions.
  - Working on regular files.
- Standard library
  - What if some have already done all that you need ?
  - os 
  - sys
  - platform
  - subprocess
  - date
  - math
  - sqlite
  - json
- Third party libraries
  - yaml
  - xmltodict
  - requests
  - telnetlib
  - ipaddress
  - netmiko
  - paramiko
  - tabulate

---
## Content

- [Basics](00_basics/README.md)
- [Scripting Intro](01_scripting_intro/README.md)
- [Parameters](02_scripting_parameters/README.md)
- [Conditions](03_scripting_conditions/README.md)
- [Loops](04_scripting_loops/README.md)
- [Arrays](05_arrays/README.md)
- [Functions](06_functions/README.md)
- [OOP](07_oop/README.md)
- [Working With Files](08_working_with_files_and_exceptions/README.md)
- [Modules And Libs](09_modules_and_libs/README.md)
- Standard Library:
  - [System Modules](10_modules/20_system_module/README.md)
  - [Network Modules](10_modules/21_net_modules/README.md)
  - [DB Modules](10_modules/22_db_modules/README.md)
- [Class Work](99_class_tasks/README.md)
- [Home Work](99_homework/README.md)

--- 

## What Does Course Aspire to ?	

The dedication of the course should be for the students who wish to get basics of programming with python language and its standard library.
## Pre-Requisites

- Linux OS (Debian or RedHat Based) is MUST.
- Version Control is nice to know.
- Web front end is nice to know.
- Database basics is nice to know.
- OSI 7 Layers